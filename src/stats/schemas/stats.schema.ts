var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const statsSchema = new Schema({
    count_mutations: Number,
    count_no_mutations: Number,
    ratio: String
});
module.exports = mongoose.model('statsSchema', statsSchema);  
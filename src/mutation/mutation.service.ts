import { Injectable } from '@nestjs/common';
import { count } from 'console';

@Injectable()
export class MutationService {

    hasMutation(infoDna: string[]){
        let Letras = ['A','T','C','G',','];

        let strInfoDna = infoDna.toString();;

        let dnaSplit = strInfoDna.split("");
        let dnaVertical = dnaToVertical(infoDna);
        let dnaObliquaIzquierda = dnaToObliquaIzquierda(infoDna)
        let dnaObliquaDerecha = dnaToObliquaDerecha(infoDna)
        
        if ( containsOnly(Letras, dnaSplit) == true){
            
            if (mutationA(infoDna).length>0
            ||mutationT(infoDna).length>0
            ||mutationC(infoDna).length>0
            ||mutationG(infoDna).length>0){
                return JSON.parse('true');
            }//Comprobar mutación en horizontal
            if (mutationA(dnaVertical).length>0
            ||mutationT(dnaVertical).length>0
            ||mutationC(dnaVertical).length>0
            ||mutationG(dnaVertical).length>0){
                return JSON.parse('true');
            }//Comprobar mutación en vertical
            if (mutationA(dnaObliquaIzquierda).length>0
            ||mutationT(dnaObliquaIzquierda).length>0
            ||mutationC(dnaObliquaIzquierda).length>0
            ||mutationG(dnaObliquaIzquierda).length>0){
                return JSON.parse('true');
            }//Comprobar mutación en obliqua / diagonal izquierda
            if (mutationA(dnaObliquaDerecha).length>0
            ||mutationT(dnaObliquaDerecha).length>0
            ||mutationC(dnaObliquaDerecha).length>0
            ||mutationG(dnaObliquaDerecha).length>0){
                return JSON.parse('true');
            }//Comprobar mutación en obliqua / diagonal derechoa
            else{
                return false;
            }            
        }else{
            return false;
        }
            

        
        function containsOnly(array1, array2){
            return !array2.some(elem => !array1.includes(elem))
        }
          
        function mutationA(infoDna:string []) {
            let mutationA = 'AAAA';
            return infoDna.filter(function (e) {
                return e.indexOf(mutationA) > -1;
            });
        }

        function mutationT(infoDna:string []) {
            let mutationT = 'TTTT';
            return infoDna.filter(function (e) {
                return e.indexOf(mutationT) > -1;
            });
        }
        function mutationC(infoDna:string []) {
            let mutationC = 'CCCC';
            return infoDna.filter(function (e) {
                return e.indexOf(mutationC) > -1;
            });
        }
        function mutationG(infoDna:string []) {
            let mutationG = 'GGGG';
            return infoDna.filter(function (e) {
                return e.indexOf(mutationG) > -1;
            });
        }

        function dnaToVertical(infoDna:string []) {
            let dnaVertical = [];
            let dnaVerticalTemp = [];

            var colLen = infoDna.length;
            var rowLen = infoDna[0].length;

            //console.log(infoDna.length) cuantos arrays tiene
            for (let i = 0; i < rowLen; i += 1) {
                for (let j = 0; j < colLen; j++) {
                    dnaVertical.push(infoDna[j][i]);
                }
                
                if(dnaVertical.length > 0) {
                    while(dnaVertical.length) dnaVerticalTemp.push(dnaVertical.splice(0,rowLen).join(''));
                }
            }
            return dnaVerticalTemp;
        }

        function dnaToObliquaIzquierda(infoDna:string []) {
            let temp = [];
            let resultado = [];

            var colLen = infoDna.length;

            for (let s=0; s<colLen; s++) {
                for (let i=s; i>-1; i--) {
                    temp.push(infoDna[i][s-i]);
                }
                temp.push('-');
            }

            for (let s=1; s<colLen; s++) {
                for (let i=colLen-1; i>=s; i--) {
                    temp.push(infoDna[i][s+colLen-1-i]);
                }
                temp.push('-');
            }
            
            if(temp.length > 0) {
                while(temp.length) resultado.push(temp.splice(0).join(''));
            }
            return resultado;

        }

        function dnaToObliquaDerecha(infoDna:string []) {
            let resultado = [];
            let temp = [];
            var colLen = infoDna.length;
            var rowLen = infoDna[0].length;
            var maxLength = Math.max(colLen, rowLen);

            for (let k = 0; k <= 2 * (maxLength - 1); ++k) {
                temp = [];
                for (let y = colLen - 1; y >= 0; --y) {
                    let x = k - (colLen - y);
                    temp.push(infoDna[y][x]);
                }
                //temp.push('-');
                if(temp.length > 0) {
                    while(temp.length) resultado.push(temp.splice(0).join(''));
                }
                
            }
            return resultado;

        }
    }
}
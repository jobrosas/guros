import { Body, Controller, Get, Post } from '@nestjs/common';
import { AddDnaDto } from './dto/add-dna.dto';
import { MutationService } from './mutation.service';
import { HttpException } from '@nestjs/common';
import { IsArray, IsNotEmpty } from 'class-validator';
import { StatsController } from '../stats/stats.controller';


@Controller('mutation')
export class MutationController {

    constructor (private mutationService: MutationService) {}

    @IsNotEmpty()
    @IsArray()
    @Post()
    
    showDna(@Body() infoDna: AddDnaDto) {

        if (this.mutationService.hasMutation(infoDna.dna)==true){
            StatsController.stats(true);
            throw new HttpException("Hay mutación", 200);
        }else{
            StatsController.stats(false);
            throw new HttpException("No se encontró mutación", 403);
        }
         
    }

    
    
}

import { IsArray, IsNotEmpty, IsString } from 'class-validator';
export class AddDnaDto {
    @IsNotEmpty()
    @IsArray()
    dna: string[];
}
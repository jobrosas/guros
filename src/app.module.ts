import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MutationController } from './mutation/mutation.controller';
import { MutationService } from './mutation/mutation.service';
import { StatsController } from './stats/stats.controller';
import { StatsService } from './stats/stats.service';

@Module({
  imports: [],
  controllers: [AppController, MutationController, StatsController],
  providers: [AppService, MutationService, StatsService],
})
export class AppModule {}


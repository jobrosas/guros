import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //await await app.listen(3000);

  var port = process.env.PORT || 3000;

  var server = app.listen(port, function () {
      console.log('Server running at http://127.0.0.1:' + port + '/');
  });
}
bootstrap();


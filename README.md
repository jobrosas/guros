# ¡Hola!

# Instalación
Instrucciones para ejecutar local éste repositorio:

1.- Descarga o clona el respositorio desde:


    git@gitlab.com:jobrosas/desafuioguros.git 
ó desde
    https://gitlab.com/jobrosas/desafuioguros.git

2.- Tener instalado <a href="https://nodejs.org/">Node.js</a> 10.13 o superior

3.- Tener instalado <a href="https://nextjs.org/">Nest.js</a> 7.5 o superior

4.- Una vez instalado, corre la aplicación desde la terminal con el comando <i><b>npm start</b></i>

# Ejecución local

Para saber si existe mutacion deberás mandar un string [] mediante POST a: http://localhost:3000/mutation

La función para regresar si hay o no mutación es: <i><b>hasMutation(dna: String [])</b></i>, esta regresará en caso de una mutación 200, en caso contrario un 403.

Para saber los ADNs verificados con la API, mediante GET a: http://localhost:3000/stats, este regresa un JSON con las estadísticas de las verificaciones de ADN.





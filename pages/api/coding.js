import { connect } from "../../utils/database";

export default async (req, res) => {
  const { db } = await connect();
  const movies = await db
    .collection("sample_airbnb")
    .find({})
    .sort({ metacritic: -1 })
    .limit(20)
    .toArray();
  res.json(movies);
};